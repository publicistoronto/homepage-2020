// dependencies
var gulp = require('gulp');
var sass = require('gulp-sass');


gulp.task('styles', () => {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
});


gulp.task('default', () => {
    gulp.watch('app/scss/**/*.scss', (done) => {
        gulp.series(['styles'])(done);
    });
});